package main

import (
	"ez-pi/pcf8591"
	"fmt"
	"log"
	"periph.io/x/conn/v3/i2c/i2creg"
	"periph.io/x/host/v3"
	"time"
)

func main() {
	// 加载所有驱动
	if _, err := host.Init(); err != nil {
		log.Fatal(err)
	}

	// 打开第一个可用I²C总线的handle
	bus, err := i2creg.Open("/dev/i2c-1")
	//bus, err := i2creg.Open("")
	if err != nil {
		log.Fatal(err)
		return
	}
	defer bus.Close()

	// 通过执行命令 'sudo i2cdetect -y 1' 查看 PCF8591 设备地址
	dev, err := pcf8591.NewI2C(bus, 0x48)
	if err != nil {
		log.Fatal(err)
		return
	}

	run := true
	go func() {
		for run {
			v := dev.Read(pcf8591.AIO0)
			//log.Println(fmt.Sprintf("v = %d", v))
			//v = dev.Read(pcf8591.AIO0)
			//log.Println(fmt.Sprintf("v = %d", v))
			time.Sleep(1 * time.Millisecond)
			v1 := dev.Read(pcf8591.AIO1)
			time.Sleep(1 * time.Millisecond)
			v2 := dev.Read(pcf8591.AIO2)
			log.Println(fmt.Sprintf("v = %d,\tv1 = %d,\tv2 = %d", v, v1, v2))
			time.Sleep(1 * time.Millisecond)
		}
	}()
	time.Sleep(20 * time.Second)
	run = false
}

// Uint16ToBytes unit16 -> bytes
func Uint16ToBytes(n uint16) []byte {
	return []byte{
		byte(n),
		byte(n >> 8),
	}
}

// BytesToUint16 bytes -> unit16
func BytesToUint16(array []byte) uint16 {
	var data uint16 = 0
	for i := 0; i < len(array); i++ {
		data = data + uint16(uint(array[i])<<uint(8*i))
	}

	return data
}
