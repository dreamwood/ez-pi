package main

import (
	displayRGB "github.com/PaulB2Code/lcd-1602-uart-i2c"
	"golang.org/x/exp/io/i2c"
	"log"
	"time"
)

func main() {
	d, err := displayRGB.Open(&i2c.Devfs{Dev: "/dev/i2c-1"}, 0x27)
	if err != nil {
		log.Fatal(err)
	}
	defer d.Close()
	err = d.InitLCD()
	if err != nil {
		log.Fatal(err)
	}
	for true {
		now := time.Now()
		//第一行显示年月日0.

		d.LcdString(now.Format("2006-01-02"), displayRGB.LCD_LINE_1)
		d.LcdString(now.Format("15:04:05"), displayRGB.LCD_LINE_2)
		time.Sleep(10 * time.Millisecond)
	}
}
