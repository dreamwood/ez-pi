module ez-pi

go 1.19

require (
	github.com/PaulB2Code/lcd-1602-uart-i2c v0.0.0-20170404125915-1b340f48fc69
	github.com/brian-armstrong/gpio v0.0.0-20181227042754-72b0058bbbcb
	github.com/fsnotify/fsnotify v1.7.0
	golang.org/x/exp v0.0.0-20240909161429-701f63a606c0
)

require (
	golang.org/x/sys v0.4.0 // indirect
	periph.io/x/conn v0.0.2 // indirect
	periph.io/x/conn/v3 v3.7.0 // indirect
	periph.io/x/host v0.0.1 // indirect
	periph.io/x/host/v3 v3.8.2 // indirect
)
